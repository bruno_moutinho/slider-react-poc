class VideoMiniature extends React.Component {
	render() {
		let id = this.props.data.id
		let imgSrc = 'http://img.youtube.com/vi/' + id + '/mqdefault.jpg'
		return(
			<div className='slider__list__miniature' onClick={ this.props.clickMiniature.bind(this, id) }>
				<img src={ imgSrc } />
			</div>
		)
	}
}

class List extends React.Component {
	render() {
		let clickMiniature = this.props.clickMiniature
		let list = this.props.data.map((video, i) =>
			<VideoMiniature data={ video } clickMiniature={ clickMiniature } key={ i } />
		)

		return(
			<div>
				<div className='slider__list__previous' onClick={ this.props.shiftLeft }>
					<span>&lt;</span>
				</div>
				<div className='slider__list' >
					{ list }
				</div>
				<div className='slider__list__next' onClick={ this.props.shiftRight }>
					<span>&gt;</span>
				</div>
			</div>
		)
	}
}

class Slider extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			currentVideo: '',
			videos: [],
			videosInDisplay: {
				begin: '',
				end: '',
				videos: []
			}
		}
	}
	componentDidMount() {
		let videos = [
			{	id: 'QH2-TGUlwu4'	},
			{	id: '4IP_E7efGWE'	},
			{	id: '8HVWitAW-Qg'	},
			{	id: 'SSnelmtdzGQ'	},
			{	id: 'YZs3EADi2pQ'	},
			{	id: 'P4W1VSb-dGU'	},
			{	id: 'siA-6PblJCA'	},
			{ id: 'jEzbqS5HroA' },
			{ id: 'QUXhoZFDwQg' },
			{ id: 'HECa3bAFAYk' },
			{ id: 'SlxdAJmeLcQ' },
			{ id: 'D36JUfE1oYk' },
			{ id: '1Rj8sdrCDY4' },
			{ id: 'Dnx0z1cC8u8' }
		]
		this.setState({ videos: videos, currentVideo: videos[0].id, videosInDisplay: { begin: 0, end: 6, videos: videos.slice(0,6) }})
	}
	setStateDisplay(newBegin, newEnd) {
		this.setState({
			videosInDisplay: {
				begin: newBegin,
				end: newEnd,
				videos: this.state.videos.slice(newBegin, newEnd)
			}
		})
	}
	shiftLeft() {
		let begin = this.state.videosInDisplay.begin
		let newBegin, newEnd

		if(begin == 0)
			return
		else if(begin >= 6) {
			newBegin = this.state.videosInDisplay.begin - 6
			newEnd = newBegin + 6
		} else {
			newBegin = 0
			newEnd = 6
		}
		this.setStateDisplay(newBegin, newEnd)
	}
	shiftRight() {
		let length = this.state.videos.length
		let newBegin, newEnd


		if(length <= 6)
			return
		else if(length >= this.state.videosInDisplay.end + 6) {
			newBegin = this.state.videosInDisplay.end
			newEnd = this.state.videosInDisplay.end + 6
		} else {
			newEnd = length
			newBegin = newEnd - 6
		}

		this.setStateDisplay(newBegin, newEnd)
	}
	clickMiniature() {
		let slider = this
		return function(id) {
			slider.setState({ currentVideo: id })
		}	
	}
	render() {
		let currentVideoURL = 'https://www.youtube.com/embed/' + this.state.currentVideo
		return (
			<div>
				<div className='display'>
					<section className="display__video">
						<iframe className='current-video' width='700px' height='393.75px' src={ currentVideoURL } frameBorder="0" ></iframe> 
					</section>
				</div>
				<div className='slider'>
					<div className="slider__container">
						<List data={ this.state.videosInDisplay.videos } shiftLeft={ this.shiftLeft.bind(this) } shiftRight={ this.shiftRight.bind(this) } clickMiniature={ this.clickMiniature() } />
					</div>
				</div>
			</div>
		)
	}
}

ReactDOM.render(
	<Slider />,
	document.getElementById('slider-container')
);